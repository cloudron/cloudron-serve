#!/usr/bin/env node

'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    path = require('path'),
    ldapjs = require('ldapjs'),
    session = require('express-session'),
    mustacheExpress = require('mustache-express');

function exit(error) {
    if (error) console.error(error);
    process.exit(error ? 1 : 0);
}

if (!process.env.CLOUDRON_LDAP_URL || !process.env.CLOUDRON_LDAP_USERS_BASE_DN) {
    exit('No LDAP_URL or LDAP_USERS_BASE_DN configured.');
}

var port = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;
var root = process.argv[2] || path.resolve('public');

var app = express();

app.engine('html', mustacheExpress());
app.set('views', path.join(__filename, '../views'));
app.set('view engine', 'html');
app.set('trust proxy', 1);
app.use(session({
    secret: 'cloudron is the big deal',
    resave: false,
    saveUninitialized: false,
    cookie: {}
}));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/healthcheck', function (req, res) {
    res.status(200).end();
});

app.post('/login', function (req, res) {
    if (!req.body.username || typeof req.body.username !== 'string') return res.render('login', { error: 'username must be non empty string' });
    if (!req.body.password || typeof req.body.password !== 'string') return res.render('login', { error: 'password must be non empty string' });

    var ldapClient = ldapjs.createClient({ url: process.env.CLOUDRON_LDAP_URL });
    var ldapDn = 'cn=' + req.body.username + ',' + process.env.CLOUDRON_LDAP_USERS_BASE_DN;

    ldapClient.bind(ldapDn, req.body.password, function (error) {
        if (error) return res.render('login', { error: 'Invalid credentials' });

        req.session.valid = true;
        res.redirect('/');
    });
});

app.get('/_logout', function (req, res) {
    req.session.valid = false;
    res.redirect('/');
});

app.use(function (req, res, next) {
    if (!req.session || !req.session.valid) return res.render('login', { error: '' });
    next();
});

app.use(express.static(root));

app.listen(port, function () {
    console.log('Listening on port %s', port);
    console.log('Serving up directory %s', root);
});
