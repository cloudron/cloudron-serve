# The Cloudron Serve Tool

The [Cloudron](https://cloudron.io) serve tool is basically like [serve](https://www.npmjs.com/package/serve)
and other popular file serving tools. The difference is, that once running on a Cloudron, it will authenticate with LDAP.

## Installation

Installing the CLI tool requires [node.js](https://nodejs.org/) and
[npm](https://www.npmjs.com/). The CLI tool can be installed using the
following command:

```
npm install -g cloudron-serve
```

Depending on your setup, you may need to run this as root.

You should now be able to run the `cloudron help` command in a shell.


## Usage
```
$ PORT=1337 cloudron-serve /path/to/public/folder
```
